# ansible-role-keycloak

Role install open-jdk and Keycloak on deb-based servers

'''

keycloak_user: keycloak
keycloak_group: keycloak
keycloak_tmp_dir: /tmp
keycloak_dir: /opt/keycloak

'''
